
# C-RAID



## Projet de Recherche CAPARENV
### WP7.1 Analyse des résultats de la preuve de concept : identification de la perte des ancrages sur les bouées dérivantes





## C-RAID data
The C-RAID data are archived with [DOI 10.17882/77184](https://doi.org/10.17882/77184) in [seanoe.org](https://www.seanoe.org/data/00660/77184/)

Differents data version can be downloaded ( 86062.zip, 98118.zip, 102039.zip...)

Unzip the downloaded file, and then recursively unzip all files. Use Script/#1-Unziping.sh for example.



# Installation

## installation sur datarmor

Une fois connecté à datarmor ```ssh datarmor``` activer conda puis continuer l'installation (cf ci-après)

```bash
source /appli/anaconda/versions/2022.10/etc/profile.d/conda.csh
```


## installation sur pc

Dans un terminal installer conda puis continuer l'installation (cf ci-après)
```bash
sudo apt-get install -y --no-install-recommends conda
```

## installation

Permet de créer l'environnement conda c-raidEnv, d'installer les paquets R conda disponibes, puis de compiler les paquets R complémentaires.

```bash


git clone https://gitlab.ifremer.fr/jb06098/c-raid
cd c-raid

conda init bash
conda create --name c-raidEnv python=3.8
conda activate c-raidEnv

conda install --name c-raidEnv r-ncdf4 r-doparallel r-dplyr r-oce r-lubridate r-tidyverse r-data.table r-snow 

#trackreconstruction dependencies
conda install --name c-raidEnv r-rcolorbrewer r-fields

#SpatialEpi dependencies
conda install --name c-raidEnv r-sp r-rcpparmadillo r-spdata r-sf r-deldir r-boot r-terra r-datetime r-geosphere r-rmpfr proj gdal  pkg-config gmp gsl cmake
conda install --name c-raidEnv  -c conda-forge  udunits2 liblapack libblas  r-rgdal  r-devtools


Rscript "Scripts/#0-packages-install.R"
```

# Utilisation



## variables d'environnement

Deux variables d'environnement sont utilisées
  C_RAID_DATA = path to the unzipped version of the downloaded data
  C_RAID_OUTPUT = path to write script result

De plus, le fichier #2-Contact.R est à modifier du au manque de cohérence entre les différentes livraions de données du projet C-RAID
  le nom du fichier contenant les identifiants de bouée et le nom du fichier contenant la correspondance le numéro de bouée et l'identifiant Argo sont à modifier
  cela peut nécessiter de modifier le fichier xlsx en csv

## Utilisation sur datarmor

Le fichier pbs/c-raid.pbs permet de lancer les différentes scripts R

Le fichier pbs/c-raid.pbs contient les variables d'environnement à configurer : C_RAID_DATA et  C_RAID_OUTPUT


```bash
qsub pbs/c-raid.pbs
```

## Utilisation sur pc


```bash

conda activate c-raidEnv

export C_RAID_DATA=<path>/DATA
export C_RAID_OUTPUT=<path>

Rscript "Scripts/<file>.R"
```



# Scripts initiaux fournit par Capgemini
Les scripts initiaux fournit par Capgemini ainsi que les résultats et les réseaux neurones entraînés XGBoost sont archivés de façon perenne avec le logiciel APRIM dans
General/IDM/SISMER/Coriolis/Livraison_C-RAID_POC_DROGUE_IA.zip
