# get shiny server and R from the rocker project
ARG R_VERSION=4.1.0
ARG IMAGE_REGISTRY
ARG DOCKER_IMAGE=rocker/rstudio:$R_VERSION
FROM $IMAGE_REGISTRY$DOCKER_IMAGE AS prepare

# system libraries
# Try to only install system libraries you actually need
# Package Manager is a good resource to help discover system deps
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        # shiny dependencies
        libssl-dev libcurl4-gnutls-dev libxml2-dev \
        # application dependencies
        libudunits2-dev libproj-dev libgdal-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

FROM prepare AS install
# create app directory
RUN mkdir /build
WORKDIR /build
# copy DESCRIPTION fisrt and install dependencies
# usefull to speed up the image's build
COPY DESCRIPTION .
RUN R -e 'install.packages("remotes"); remotes::install_cran("attachment"); attachment::install_from_description()'

# copy application and install package
RUN mkdir /app
WORKDIR /app
COPY . .
#RUN R -e 'remotes::install_local(upgrade="never")'

FROM install AS run


