

# download and unzip C-RAID 2021Q1 winter delivery
cd ${C_RAID_DATA_PATH}
wget https://www.seanoe.org/data/00660/77184/data/86062.zip
unzip 86062.zip
rm 86062.zip #for save space after unzipping
cd C-RAID_Phase2_SpringDelivery/DATA/
unzip data.zip
rm data.zip #for save space after unzipping
cd data
# unzipping complementary_data
#TODO parallel unzip of complementary_data data files
find . -type f -name "*.zip" -exec sh -c 'unzip -n -d "`dirname \"{}\"`" "{}"' ';' -exec rm -v {} \;
find . -type f -name "*READ_ME.txt" -exec rm -v {} \;



# Download and unzip 2022Q2
cd ${C_RAID_DATA_PATH}
wget https://www.seanoe.org/data/00660/77184/data/95288.zip
unzip 95288.zip
rm 95288.zip  #for save space after unzipping
cd C-RAID_MCO_CORIOLIS_SPRING_DELIVERY_2022/DATA/
unzip data.zip
rm data.zip #for save space after unzipping
cd data
# unzipping complementary_data
#TODO parallel unzip of complementary_data data files
find . -type f -name "*.zip" -exec sh -c 'unzip -n -d "`dirname \"{}\"`" "{}"' ';' -exec rm -v {} \;
find . -type f -name "*READ_ME.txt" -exec rm -v {} \;
```

